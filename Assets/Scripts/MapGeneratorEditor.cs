﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapGenerator))]
class MapGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        MapGenerator myTarget = (MapGenerator)target;

        if (GUILayout.Button("Generate Map"))
        {
            myTarget.GenerateMap();
        }
    }
}
