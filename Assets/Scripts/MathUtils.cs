﻿using UnityEngine;

public class MathUtils
{
    public static int GetTotalDistanceOfMove(Vector2Int oldPosition, Vector2Int newPosition)
    {
        Vector2Int res = oldPosition - newPosition;

        int absX = (int)Mathf.Abs(res.x);
        int absY = (int)Mathf.Abs(res.y);

        // Using just the bigger axis means diagonal movement counts as the same
        int result = absX + absY;

        return result;
    }

    //public static int GetTotalDistance(Vector2Int vecA, Vector2Int vecB)
    //{
    //    int xDiff = Mathf.Abs(vecA.x - vecB.x);
    //    int yDiff = Mathf.Abs(vecA.y - vecB.y);

    //    return xDiff + yDiff;
    //}
}