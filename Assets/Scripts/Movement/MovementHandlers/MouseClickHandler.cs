﻿using UnityEngine;

public class MouseClickHandler : MonoBehaviour, MovementInputHandler
{
    private Vector2Int _coordinatesHit;

    public bool DoesMovementHappen()
    {
        return DidMouseClickAndHitSomething();
    }

    public bool DidMouseClickAndHitSomething()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit);

            if (hit.collider != null)
            {
                MapTile tile = hit.collider.gameObject.GetComponent<MapTile>();
                if (tile != null)
                {
                    _coordinatesHit = tile.Coordinates;
                    //Debug.LogError(tile.Coordinates);
                    return true;
                }
            }
        }

        return false;
    }

    public Vector2Int GetMovementLocation()
    {
        return _coordinatesHit;
    }

    public void Update() {}
}
