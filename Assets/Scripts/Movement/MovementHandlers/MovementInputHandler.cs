﻿using UnityEngine;

public interface MovementInputHandler
{
    bool DoesMovementHappen();
    Vector2Int GetMovementLocation();
    void Update();
}