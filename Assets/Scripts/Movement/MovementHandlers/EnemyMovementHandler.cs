﻿using UnityEngine;

public class EnemyMovementHandler : MonoBehaviour, MovementInputHandler
{
    [SerializeField]
    private Character _enemyEntity;

    [SerializeField]
    private MapData _mapData;

    private Vector2Int CoordinatesHit;

    private float _moveTimer;

    private bool _movementReady;

    private enum Direction
    {
        LEFT = 0,
        RIGHT = 1,
        UP = 2,
        DOWN = 3
    }

    public bool DoesMovementHappen()
    {
        return true;
    }

    public Vector2Int GetMovementLocation()
    {
        return GetNextMovementLocation();
    }

    private Vector2Int GetNextMovementLocation()
    {
        Vector2Int currentPos = _enemyEntity.MapPosition;

        int xMax = (_mapData.TileGameObjects.GetLength(0) - 1);
        int yMax = (_mapData.TileGameObjects.GetLength(1) - 1);

        Direction dir = Direction.LEFT;
        while (FindValidDirection(currentPos, xMax, yMax, ref dir) == false);

        Vector2Int nextPos = currentPos;

        switch(dir)
        {
            case Direction.LEFT:
                nextPos.x -= _enemyEntity.MovementSpeed;
                break;

            case Direction.RIGHT:
                nextPos.x += _enemyEntity.MovementSpeed;
                break;

            case Direction.UP:
                nextPos.y += _enemyEntity.MovementSpeed;
                break;

            case Direction.DOWN:
                nextPos.y -= _enemyEntity.MovementSpeed;
                break;
        }

        return nextPos;
    }

    private bool FindValidDirection(Vector2Int currentPos, int xMax, int yMax, ref Direction dir)
    {
        dir = (Direction)Random.Range(0, 4);

        int distanceFromXEdge = xMax - currentPos.x;
        int distanceFromYEdge = yMax - currentPos.y;

        if (dir == Direction.RIGHT && distanceFromXEdge < _enemyEntity.MovementSpeed)
        {
            return false;
        }

        if (dir == Direction.LEFT && currentPos.x < _enemyEntity.MovementSpeed)
        {
            return false;
        }

        if (dir == Direction.UP && distanceFromYEdge < _enemyEntity.MovementSpeed)
        {
            return false;
        }

        if (dir == Direction.DOWN && currentPos.y < _enemyEntity.MovementSpeed)
        {
            return false;
        }

        return true;
    }

    public void Update()
    {
        /*_movementReady = false;
        _moveTimer += Time.deltaTime;

        if (_moveTimer >= _enemyEntity.TimeToMove)
        {
            _moveTimer = 0f;
            _movementReady = true;
        }*/
    }
}