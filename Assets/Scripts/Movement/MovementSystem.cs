﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using DigitalRuby.Tween;

public class MovementSystem : MonoBehaviour
{
    [SerializeField]
    private Character _character;

    [SerializeField]
    private MouseClickHandler _mouseClickHandler;

    [SerializeField]
    private MapData _mapData;

    [SerializeField]
    private MapMovementRangeIndicator _rangeIndicator;

    //private Vector2Int[] _movementPath;

    //private Vector2Int _previousPosition = Vector2Int.zero;

    private void Update()
    {
        if(_mouseClickHandler.DoesMovementHappen())
        {
            MoveCharacter();
        }
    }

    public float _moveDuration = 0.5f;

    private void MoveCharacter()
    {
        //FindPathToLocation(_character.MapPosition + new Vector2Int(3, 1));

        List<MapTile> pathToGoal = FindPath(_character.MapPosition, _mouseClickHandler.GetMovementLocation());

        _character.MapPosition = _mouseClickHandler.GetMovementLocation();


        StartCoroutine(TweenCharacterAlongPath(pathToGoal));
        

        //_character.gameObject.Tween("MoveCharacter", _character.transform.position, mapPosition, _moveDuration, TweenScaleFunctions.CubicEaseIn, updateCharacterPos);

        //_character.transform.position = mapPosition;

        //_rangeIndicator.ColorInRangeTiles(_character.MapPosition);
    }

    private IEnumerator TweenCharacterAlongPath(List<MapTile> pathToGoal)
    {
        //Vector3 mapPosition = _mapData.TileGameObjects[_character.MapPosition.x, _character.MapPosition.y].transform.position;
        //mapPosition.y = _character.transform.position.y;

        System.Action<ITween<Vector3>> updateCharacterPos = (t) =>
        {
            _character.gameObject.transform.position = t.CurrentValue;
        };

        for (int i = 0; i < pathToGoal.Count; ++i)
        {
            Vector3 newCharPosition = pathToGoal[i].transform.position;
            newCharPosition.y = _character.transform.position.y;
            _character.gameObject.Tween("MoveCharacter", _character.transform.position, newCharPosition, _moveDuration, TweenScaleFunctions.CubicEaseIn, updateCharacterPos);
            yield return new WaitForSeconds(_moveDuration);
        }

        _rangeIndicator.ColorInRangeTiles(_character.MapPosition);
    }

    public Vector2Int GoalPosition = new Vector2Int(2, 2);
    private void OnGUI()
    {
        if(GUI.Button(new Rect(0f, 0f, 100f, 100f), "FindPathToGoal"))
        {
            FindPath(_character.MapPosition, GoalPosition);
        }
    }

    private List<MapTile> _openList = new List<MapTile>();
    private List<MapTile> _closedList = new List<MapTile>();

    public int breakoutNum = 30;

    private List<MapTile> FindPath(Vector2Int startCoordinates, Vector2Int goalCoordinates)
    {
        _openList.Clear();
        _closedList.Clear();

        MapTile tile = _mapData.Tiles[startCoordinates.x, startCoordinates.y];
        tile.SetPathfindingData(0, 0, null);

        _closedList.Add(tile);

        List<MapTile> tilesSurroundingStartTile = GetSurroundingTiles(tile, tile, goalCoordinates);
        _openList.AddRange(tilesSurroundingStartTile);

        MapTile goalTile = _mapData.Tiles[goalCoordinates.x, goalCoordinates.y];


        int breakoutCounter = 0;
        

        while (!_closedList.Contains(goalTile))
        {
            ++breakoutCounter;
            if(breakoutCounter >= breakoutNum)
            {
                for (int i = 0; i < _openList.Count; ++i)
                {
                    _openList[i].Colour();
                }
                return null;
            }

            bool hasTies = false;
            List<MapTile> currentTies = new List<MapTile>();

            int lowestCostIndex = 0;
            int lowestFCost = int.MaxValue;
            for (int i = 0; i < _openList.Count; ++i)
            {
                if(_openList[i] == goalTile)
                {
                    _closedList.Add(goalTile);
                    break;
                }

                if(_openList[i].FCost < lowestFCost)
                {
                    hasTies = false;
                    currentTies.Clear();

                    lowestCostIndex = i;
                    lowestFCost = _openList[i].FCost;
                }
                else if(_openList[i].FCost == lowestFCost)
                {
                    if(!hasTies)
                    {
                        currentTies.Add(_openList[lowestCostIndex]);
                    }

                    hasTies = true;
                    currentTies.Add(_openList[i]);
                }
            }

            MapTile newActiveTile;

            if(hasTies)
            {
                newActiveTile = currentTies[currentTies.Count - 1];
            }
            else
            {
                newActiveTile = _openList[lowestCostIndex];
            }

            _closedList.Add(newActiveTile);
            _openList.Remove(newActiveTile);
            _openList.AddRange(GetSurroundingTiles(newActiveTile, tile, goalTile.Coordinates));
        }

        Debug.LogError("found it!");

        //MapTile currentTile = goalTile;
        //MapTile currentParent = goalTile.Parent;
        //goalTile.Colour();
        //StartCoroutine(ColourPathCoroutine(goalTile));
        /*while (currentParent != null)
        {
            currentTile.Colour();
            currentParent = currentTile.Parent;
            currentTile = currentParent;
        }*/

        return BuildPathFromGoal(goalTile);
    }

    private List<MapTile> BuildPathFromGoal(MapTile goalTile)
    {
        List<MapTile> pathFromGoal = new List<MapTile>();

        MapTile currentTile = goalTile;

        while (currentTile.Parent != null)
        {
            pathFromGoal.Add(currentTile);
            currentTile = currentTile.Parent;
        }

        pathFromGoal.Reverse();

        return pathFromGoal;
    }

    public float _timeBetweenPathColouring = 0.2f;

    private IEnumerator ColourPathCoroutine(MapTile goalTile)
    {
        List<MapTile> pathFromGoal = BuildPathFromGoal(goalTile);

        for(int i = 0; i < pathFromGoal.Count; ++i)
        {
            yield return new WaitForSeconds(_timeBetweenPathColouring);
            pathFromGoal[i].Colour();
        }

        /*while (currentParent != null)
        {
            yield return new WaitForSeconds(_timeBetweenPathColouring);
            currentTile.Colour();
            currentParent = currentTile.Parent;
            currentTile = currentParent;
        }*/
    }

    private List<MapTile> GetSurroundingTiles(MapTile centralTile, MapTile startPos, Vector2Int endPos)
    {
        List<MapTile> surroundingTiles = new List<MapTile>();

        // This is not on the right edge
        if (centralTile.x + 1 != _mapData.Tiles.GetLength(0))
        {
            surroundingTiles.Add(_mapData.Tiles[centralTile.x + 1, centralTile.y]);
        }

        // It's not on the left edge
        if (centralTile.x > 0)
        {
            surroundingTiles.Add(_mapData.Tiles[centralTile.x - 1, centralTile.y]);
        }
        
        // Not on the upper edge
        if (centralTile.y < _mapData.Tiles.GetLength(1) - 1)
        {
            surroundingTiles.Add(_mapData.Tiles[centralTile.x, centralTile.y + 1]);
        }

        // Not on the bottom edge
        if (centralTile.y > 0)
        {
            surroundingTiles.Add(_mapData.Tiles[centralTile.x, centralTile.y - 1]);
        }

        for(int i = surroundingTiles.Count - 1; i >= 0; --i)
        {
            if(_closedList.Contains(surroundingTiles[i]) || surroundingTiles[i].IsWall)
            {
                if(surroundingTiles[i].IsWall)
                {
                    Debug.LogError("is wall, coords: " + surroundingTiles[i].Coordinates);
                }
                surroundingTiles.Remove(surroundingTiles[i]);
            }
            else
            {
                int gCost = centralTile.GCost + 1; //MathUtils.GetTotalDistanceOfMove(startPos.Coordinates, centralTile.Coordinates);
                int hCost = MathUtils.GetTotalDistanceOfMove(endPos, centralTile.Coordinates);
                surroundingTiles[i].SetPathfindingData(gCost, hCost, centralTile);
            }
        }

        return surroundingTiles;
    }

    //public int LoopCutoffNum = 10;

    //private void FindPathToLocation(Vector2Int goalPos)
    //{
    //    int breakoutNum = 0;
    //    Vector2Int nextCoords = _character.MapPosition;
    //    Vector2Int temp;
    //    while (nextCoords != goalPos)
    //    {
    //        breakoutNum++;

    //        temp = FindNextCoords(nextCoords, goalPos);
    //        _previousPosition = nextCoords;
    //        nextCoords = temp;
    //        Debug.LogError("nextcoords: " + nextCoords);

    //        if (breakoutNum >= LoopCutoffNum)
    //        {
    //            break;
    //        }
    //    }
    //}

    //private Vector2Int FindNextCoords(Vector2Int currentPos, Vector2Int goalPos)
    //{
    //    // Gonna use A*

    //    Vector2Int startPosition = _character.MapPosition;

    //    int rightFCost = int.MaxValue;
    //    int leftFCost = int.MaxValue;
    //    int upFCost = int.MaxValue;
    //    int downFCost = int.MaxValue;

    //    Vector2Int rightCoords = Vector2Int.zero;
    //    Vector2Int leftCoords = Vector2Int.zero;
    //    Vector2Int upCoords = Vector2Int.zero;
    //    Vector2Int downCoords = Vector2Int.zero;

    //    if (currentPos.x < _mapData.TileGameObjects.GetLength(0) - 1)
    //    {
    //        rightCoords = _mapData.Tiles[currentPos.x + 1, currentPos.y].Coordinates;

    //        if (rightCoords != _previousPosition)
    //        {
    //            rightFCost = CalculateFCostForNode(rightCoords, startPosition, goalPos);
    //        }
    //    }

    //    if (currentPos.x > 0)
    //    {
    //        leftCoords = _mapData.Tiles[currentPos.x - 1, currentPos.y].Coordinates;

    //        if (leftCoords != _previousPosition)
    //        {
    //            leftFCost = CalculateFCostForNode(leftCoords, startPosition, goalPos);
    //        }
    //    }

    //    if (currentPos.y < _mapData.TileGameObjects.GetLength(1) - 1)
    //    {
    //        upCoords = _mapData.Tiles[currentPos.x, currentPos.y + 1].Coordinates;

    //        if (upCoords != _previousPosition)
    //        {
    //            upFCost = CalculateFCostForNode(upCoords, startPosition, goalPos);
    //        }
    //    }

    //    if (currentPos.y > 0)
    //    {
    //        downCoords = _mapData.Tiles[currentPos.x, currentPos.y - 1].Coordinates;

    //        if (downCoords != _previousPosition)
    //        {
    //            downFCost = CalculateFCostForNode(downCoords, startPosition, goalPos);
    //        }
    //    }

    //    Debug.LogErrorFormat("right: {0} left: {1} up: {2} down: {3}", rightFCost, leftFCost, upFCost, downFCost);

    //    Vector2Int smallestFCostCoords = Vector2Int.zero;
    //    int smallestFCost = Mathf.Min(rightFCost, leftFCost, upFCost, downFCost);

    //    if(smallestFCost == rightFCost)
    //    {
    //        smallestFCostCoords = rightCoords;
    //    }
    //    else if(smallestFCost == leftFCost)
    //    {
    //        smallestFCostCoords = leftCoords;
    //    }
    //    else if (smallestFCost == upFCost)
    //    {
    //        smallestFCostCoords = upCoords;
    //    }
    //    else if (smallestFCost == downFCost)
    //    {
    //        smallestFCostCoords = downCoords;
    //    }

    //    return smallestFCostCoords;
    //}

    //private int CalculateFCostForNode(Vector2Int node, Vector2Int startPos, Vector2Int endPos)
    //{
    //    int distanceToStart = MathUtils.GetTotalDistanceOfMove(node, startPos);
    //    int distanceToEnd = MathUtils.GetTotalDistanceOfMove(node, endPos);

    //    return distanceToStart + distanceToEnd;
    //}
}
