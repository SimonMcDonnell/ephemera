﻿using UnityEngine;

public class MouseInputHandler : MonoBehaviour
{
    [SerializeField]
    private Material _highlightMat;

    [SerializeField]
    private Material _inRangeMat;

    private Material _normalMat;

    private bool hoverEnabled = false;

    public void ColorMeInRange(bool inRange)
    {
        hoverEnabled = inRange;

        if (hoverEnabled)
        {
            GetComponent<Renderer>().sharedMaterial = _inRangeMat;
        }
        else
        {
            GetComponent<Renderer>().sharedMaterial = _normalMat;
        }
    }

    private void Awake()
    {
        _normalMat = GetComponent<Renderer>().sharedMaterial;
    }

    private void OnMouseEnter()
    {
        MapTile mapTile = GetComponent<MapTile>();
        /*Debug.LogErrorFormat("Coords: " + mapTile.Coordinates);
        Debug.LogErrorFormat("FCost: " + mapTile.FCost);
        Debug.LogErrorFormat("GCost: " + mapTile.GCost);
        Debug.LogErrorFormat("HCost: " + mapTile.HCost);*/
        //FindPathToLocation(mapTile.Coordinates);

        if (hoverEnabled)
        {
            GetComponent<MeshRenderer>().sharedMaterial = _highlightMat;
        }
    }

    private void FindPathToLocation(Vector2Int goalPos)
    {
        // Gonna use A*

        MapTile mapTile = GetComponent<MapTile>();
        Character _character = mapTile.Character;
        MapData _mapData = mapTile.MapData;

        Vector2Int startPosition = _character.MapPosition;
        Vector2Int currentPosition = _character.MapPosition;

        int rightFCost = int.MaxValue;
        int leftFCost = int.MaxValue;
        int upFCost = int.MaxValue;
        int downFCost = int.MaxValue;

        if (currentPosition.x < _mapData.TileGameObjects.GetLength(0) - 1)
        {
            rightFCost = CalculateFCostForNode(_mapData.Tiles[currentPosition.x + 1, currentPosition.y].Coordinates, startPosition, goalPos);
        }

        if (currentPosition.x > 0)
        {
            leftFCost = CalculateFCostForNode(_mapData.Tiles[currentPosition.x - 1, currentPosition.y].Coordinates, startPosition, goalPos);
        }

        if (currentPosition.y < _mapData.TileGameObjects.GetLength(1) - 1)
        {
            upFCost = CalculateFCostForNode(_mapData.Tiles[currentPosition.x, currentPosition.y + 1].Coordinates, startPosition, goalPos);
        }

        if (currentPosition.y > 0)
        {
            downFCost = CalculateFCostForNode(_mapData.Tiles[currentPosition.x, currentPosition.y - 1].Coordinates, startPosition, goalPos);
        }

        Debug.LogErrorFormat("right: {0} left: {1} up: {2} down: {3}", rightFCost, leftFCost, upFCost, downFCost);

        //return null;
    }

    private int CalculateFCostForNode(Vector2Int node, Vector2Int startPos, Vector2Int endPos)
    {
        int distanceToStart = MathUtils.GetTotalDistanceOfMove(node, startPos);
        int distanceToEnd = MathUtils.GetTotalDistanceOfMove(node, endPos);

        return distanceToStart + distanceToEnd;
    }

    private void OnMouseExit()
    {
        if(!hoverEnabled)
        {
            return;
        }

        GetComponent<MeshRenderer>().sharedMaterial = _inRangeMat;
    }
}
