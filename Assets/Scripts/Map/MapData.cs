﻿using UnityEngine;

[CreateAssetMenu(fileName = "MapData", menuName = "MapData")]
public class MapData : ScriptableObject
{
    public GameObject[,] TileGameObjects;
    public MapTile[,] Tiles;

    public GameObject GetGameObjectAtCoordinates(int x, int y)
    {
        return TileGameObjects[x, y];
    }

    public MapTile GetTileAtCoordinates(int x, int y)
    {
        return Tiles[x, y];
    }
}