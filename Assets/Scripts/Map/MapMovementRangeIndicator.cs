﻿using UnityEngine;

public class MapMovementRangeIndicator : MonoBehaviour
{
    [SerializeField]
    private Character _mapEntity;

    [SerializeField]
    private MapData _mapData;

    private void Awake()
    {
        if(_mapEntity == null)
        {
            return;
        }

        ColorInRangeTiles(_mapEntity.MapPosition);
    }

    public void ColorInRangeTiles(Vector2Int newPos)
    {
        // Should move this to its own class, doesn't belong here. Can edit tile data, but should submit that to a diff system.
        for (int i = 0; i < _mapData.TileGameObjects.GetLength(0); i++)
        {
            for (int j = 0; j < _mapData.TileGameObjects.GetLength(1); j++)
            {
                int dist = MathUtils.GetTotalDistanceOfMove(_mapData.Tiles[i, j].Coordinates, newPos);

                MouseInputHandler inputHandler = _mapData.TileGameObjects[i, j].GetComponent<MouseInputHandler>();
                inputHandler.ColorMeInRange(dist <= _mapEntity.MovementSpeed);
            }
        }
    }
}