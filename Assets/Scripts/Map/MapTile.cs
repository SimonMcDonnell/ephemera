﻿using UnityEngine;

public class MapTile : MonoBehaviour
{
    public Vector3 WorldLocation;
    public Vector2Int Coordinates;

    // For testing
    public Character Character;
    public MapData MapData;

    public bool IsWall;

    public void Colour()
    {
        GetComponent<MeshRenderer>().material.color = Color.red;
    }

    public int x
    {
        get
        {
            return Coordinates.x;
        }
    }

    public int y
    {
        get
        {
            return Coordinates.y;
        }
    }

    public void SetPathfindingData(int gcost, int hcost, MapTile parent)
    {
        GCost = gcost;
        HCost = hcost;
        Parent = parent;
    }

    // Only used for AStar
    public int GCost;
    public int HCost;

    public int FCost { get { return GCost + HCost; } }

    public MapTile Parent;
}