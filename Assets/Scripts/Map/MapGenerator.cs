﻿using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject _tilePrefab;

    [SerializeField]
    private Vector2Int _mapSize;

    [SerializeField]
    private float _padding;

    [SerializeField]
    MapData _mapData;

    [SerializeField]
    private Character _character;

    [SerializeField]
    private bool _spawnOnAwake;

    private void Awake()
    {
        if (_spawnOnAwake)
        {
            ClearMap();
            GenerateMap();
        }
    }

    private void ClearMap()
    {
        for(int i = transform.childCount - 1; i >= 0; --i)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public void GenerateMap()
    {
        _mapData.TileGameObjects = new GameObject[_mapSize.x, _mapSize.y];
        _mapData.Tiles = new MapTile[_mapSize.x, _mapSize.y];

        Vector3 tileSize = _tilePrefab.GetComponent<BoxCollider>().size;

        Vector3 currentPos = new Vector3(0f, 0f, 0f);

        float minX = 0f;
        float minZ = 0f;
        float maxX = 0f;
        float maxZ = 0f;

        for (int i = 0; i < _mapSize.x; ++i)
        {
            for (int j = 0; j < _mapSize.y; ++j)
            {
                _mapData.TileGameObjects[j, i] = Instantiate(_tilePrefab, currentPos, _tilePrefab.transform.rotation, transform);
                MapTile tile = _mapData.TileGameObjects[j, i].AddComponent<MapTile>();
                tile.Coordinates = new Vector2Int(j, i);
                tile.WorldLocation = currentPos;
                tile.Character = _character;
                tile.MapData = _mapData;
                _mapData.Tiles[j, i] = tile;

                currentPos.x += tileSize.x + _padding;

                if(j == _mapSize.x - 1)
                {
                    maxX = _mapData.TileGameObjects[j, i].transform.position.x;
                }

                if (i == _mapSize.y - 1)
                {
                    maxZ = _mapData.TileGameObjects[j, i].transform.position.z;
                }
            }

            currentPos.x = 0f;
            currentPos.z += tileSize.y + _padding;
        }

        Rect boundingBox = Rect.MinMaxRect(minX, maxZ, maxX, minZ);

        //Camera.main.transform.position = boundingBox.center;
    }
}