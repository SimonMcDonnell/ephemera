﻿using UnityEngine;
using UnityEngine.UI;

public class TitleScreen : MonoBehaviour
{
    [SerializeField]
    private Button _startButton;

    [SerializeField]
    private GameObject _inGameObj;

    private void Start()
    {
        _startButton.onClick.AddListener(OnStartButtonActivated);
    }

    private void OnStartButtonActivated()
    {
        gameObject.SetActive(false);
        _inGameObj.SetActive(true);
    }
}
