﻿using UnityEngine;

public class GameFlowManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _titleScreen;

    private void Start()
    {
        _titleScreen.SetActive(true);
    }
}
